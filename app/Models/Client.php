<?php

namespace App\Models;

use Collator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    public function collector()
    {
        return $this->belongsTo(Collector::class);
    }
}
