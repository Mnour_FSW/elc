<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->foreignId('collector_id')->constrained('collectors');
            $table->string('name');
            $table->string('serial')->nullable();
            $table->string('longitude');
            $table->string('latitude');
            $table->string('location');
            $table->string('address');
            $table->string('phone');
            $table->string('nationality');
            $table->string('amp')->default('3');
            $table->string('type');
            $table->string('generator_type');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
